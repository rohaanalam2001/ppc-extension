// GET From EXT
chrome.runtime.onMessage.addListener(
  function (request, sender, sendResponse) {
    if (request.command == "BLOCKED CHANGED") {
      SetBlock();
      return;
    }
    if (request.command == 'SEEME') {
      NeedProductUpdate = false;
      Update.ID = -1;
      UpdateStatus();
      return;
    }
    if (request.command != '') {
      GetFor(request.command);
      sendResponse({ result: "success" });
    }
  });

var run = true;
function SetBlock() {
  chrome.storage.sync.get('Block', function (data) {
    if (data.Block != undefined) {
      if (data.Block == true) {
        run = false;
      }
      else { run = true; }
    }
  });
}
SetBlock();
var Update = { "ID": "-1" };
var NeedProductUpdate = false;
function UpdateStatus() {
  chrome.storage.sync.get('Update', function (data) {
    if (data.Update != null) {
      Update.ID = data.Update.ID;
      if (data.Update.IS != undefined)
        NeedProductUpdate = data.Update.IS;
      else
        NeedProductUpdate = false;
    }
  });
}

// SEND TO EXT
function SendMessageToExtension(Message) {
  chrome.runtime.sendMessage({ greeting: Message }, function (response) {
  });
}
function GetFor(id) {
  var text = getSelectionText();
  if (text != '') {
    if (id == 'Product') {
      chrome.storage.sync.set({ Product: text }, function () {
        chrome.storage.sync.set({ ProductUrl: window.location.href }, function () {
          SendMessageToExtension(id);
        });

      });
    }
    else if (id == 'ProdDesc') {
      chrome.storage.sync.set({ ProdDesc: text }, function () {

        SendMessageToExtension(id);
      });
    }
    else if (id == 'Manufacturer') {
      chrome.storage.sync.set({ Manufacturer: text }, function () {

        SendMessageToExtension(id);
      });
    }
    else if (id == 'Price') {
      chrome.storage.sync.set({ Price: text }, function () {

        SendMessageToExtension(id);
      });
    }
    else if (id == 'Code') {
      chrome.storage.sync.set({ Code: text }, function () {

        SendMessageToExtension(id);
      });
    }
  }
  else {
    chrome.runtime.sendMessage({ type: "ShowError" });
  }

}
function getSelectionText() {
  var text = "";
  if (window.getSelection) {
    text = window.getSelection().toString();
  } else if (document.selection && document.selection.type != "Control") {
    text = document.selection.createRange().text;
  }
  return text;
}
// SELECTION LISTENERREERER
var list = false;
var selectionImage;
document.addEventListener('selectionchange', () => {
  if (!list) {
    setTimeout(() => {
      list = false;
    }, 3000);
    list = true;
  }
});
$(document).on('mouseup', function (e) {
  var selection = getSelectionText();

  if (!selectionImage) {
    selectionImage = $('<div>').attr({
      id: 'Add_Data'
    }).html("GET SUGGESTIONS").css({
      //"color": "black",
      "position": "absolute",
      "background-color": "dodgerblue",
      "z-index": "99999",
      "max-height": "400px",
      "max-width": "900px",
      "overflow": "scroll",
    }).hide();
    $(document.body).append(selectionImage);
  }
});
$(document).on('click', function (e) {
  var txt = '';
  if (getSelectionText() != '') {
    txt = window.getSelection();
  }
  else {
    return;
  }
  if (!run) {
    return;
  }
  if (set) { selectionImage.fadeOut(); return; }
  $.ajax({
    url: 'https://chrome.daudt.co/AjaxCRUDJson.php?CATEGORIES=PCNIU',
    type: 'POST',
    data: {
      "Pro": getSelectionText()
    },
    success: function (response) {
      ul = '<style>.Seen{list-style:none;padding:0;}.LEi{border:2px solid black;border-top:none;border-right:none;border-left:none;margin:10px;}</style><ul class="Seen">';
      var fixed = response.replace(/\\/g, '');
      console.log(fixed);
      var dataM = JSON.parse(fixed);
      dataM.VAL.forEach(function (item) {
        var id = item.ID;
        var desc = item.PDesc;
        ul += '<li class="LEi"><a style="text-decoration:none;color:white" href="#" class="getter" data-id="' + id + '" Title="Update This Product">' + desc + '</a></li>';
      });
      ul += '</ul>'
      selectionImage.css({
        top: e.pageY - 30,
        left: e.pageX - 13
      }).fadeIn();
      $('#Add_Data').html(ul);
      $('#Add_Data').on("mouseover", function () {
        set = true;
      });
      $('#Add_Data').on("mouseout", function () {
        set = false;
      });
      $('.getter').on("click", function () {
        var id = $(this).attr("data-id");
        NeedProductUpdate = true;
        Update.ID = id;
        chrome.storage.sync.set({ Update: { "IS": true, "ProductID": id } }, function () {
          SendMessageToExtension("Update");
        });
        chrome.storage.sync.set({ Product: $(this).html() }, function () {
          chrome.storage.sync.set({ ProductUrl: window.location.href }, function () {
            SendMessageToExtension(id);
          });
        });
      });
    }
  })
}).on('mousedown', function (e) {
  if (selectionImage) {
    if (!set)
      selectionImage.fadeOut();
  }
});
var set = false;
var ul = '';
var evttar;
UpdateStatus();
$(document).on("click", function () {
  evttar = evttar;
  var img = '';
  img = trackClick(event.target);
  if (img != '') {
    chrome.storage.sync.set({ Img: img }, function () {
    });
  }
});
$("img").on("click", function () {
  var img = '';
  img = trackClick(this);
  if (img != '') {
    chrome.storage.sync.set({ Img: img }, function () {
    });
  }
});
function trackClick(div) {
  var img = '';
  var src = '';
  try {
    src = $(div).prop("src");
  } catch { }
  if (src == '' || src == "none") {
    src = $("img", div).attr("src");
  }
  if (src != '') {
    img = src;
  }
  return img;
}

$(document).on('keyup', function (e) {
  if (!run) {
    return;
  }
  e.preventDefault();
  var code = e.keyCode;
  var which = e.which;
  var Isctrl = e.ctrlKey;
  var IsShift = e.shiftKey;
  var IsAlt = e.altKey;
  var text = getSelectionText();
  if (text != '') {
    if (IsShift && IsAlt && code == 80) {
      chrome.storage.sync.set({ Product: text }, function () {
        chrome.storage.sync.set({ ProductUrl: window.location.href }, function () {
          SendMessageToExtension(id);
        });
      });
    }
    else if (IsShift && IsAlt && code == 68) {
      chrome.storage.sync.set({ ProdDesc: text }, function () {
        SendMessageToExtension(id);
      });
    }
    else if (IsShift && IsAlt && code == 77) {
      chrome.storage.sync.set({ Manufacturer: text }, function () {
        SendMessageToExtension(id);
      });
    }
    else if (IsShift && IsAlt && code == 67) {
      chrome.storage.sync.set({ Code: text }, function () {
        SendMessageToExtension(id);
      });
    }
    else if (IsShift && IsAlt && code == 49) {
      chrome.storage.sync.set({ Price: text }, function () {

        SendMessageToExtension(id);
      });
    }
  }
  else {
    if (IsShift && IsAlt && code == 80) {
      chrome.runtime.sendMessage({ type: "ShowError" });
    }
    else if (IsShift && IsAlt && code == 68) {
      chrome.runtime.sendMessage({ type: "ShowError" });
    }
    else if (IsShift && IsAlt && code == 77) {
      chrome.runtime.sendMessage({ type: "ShowError" });
    }
    else if (IsShift && IsAlt && code == 67) {
      chrome.runtime.sendMessage({ type: "ShowError" });
    }
    else if (IsShift && IsAlt && code == 49) {
      chrome.runtime.sendMessage({ type: "ShowError" });
    }
  }
});
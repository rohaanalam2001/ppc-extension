try{
var arr = ["Product", 'ProdDesc', 'Manufacturer', 'Price', 'Code','Img','SetType'];
function getAndSetValues(id) {
  chrome.storage.sync.get(id, function (data) {
    if(id!="Img"){
    $('#' + id).html(data[id]);
    }
    else{
      $('#' + id).html('<img src="'+data[id]+'" style="width:calc(100% - 250px);margin-left:40px;"/>');
    }
  });
}

chrome.storage.sync.get('Block', function (data) {
  if(data.Block != undefined)
  {
    if(data.Block == true){
      $('#Stop').val("STOPPED");
      $('#Stop').removeClass( "btn-success" ).addClass( "btn-danger" );
    }
    
  }
  boolBlocked = data.Block;
});
var boolBlocked;
$('#Stop').on('click',function(e){
  chrome.storage.sync.clear();
  if($('#Stop').val() == "STOPPED")
  {
    chrome.storage.sync.set({Block:false}, function (data) {
      $('#Stop').val("RUNNING");
      $('#Stop').removeClass( "btn-danger" ).addClass( "btn-success" );
      boolBlocked = false;
    });
  }
  else{
    chrome.storage.sync.set({Block:true}, function (data) {
      $('#Stop').val("STOPPED");
      $('#Stop').removeClass( "btn-success" ).addClass( "btn-danger" );
      boolBlocked = true;
    });
  }
  SendMessageInCurrentTAB("BLOCKED CHANGED");

  setTimeout(() => {
    window.location.reload();
  },1000);
});

arr.forEach(function (item, index) {
  chrome.storage.sync.get(item, function (data) {
    if(item!="Img" && item!='SetType'){
    $('#' + item).html(data[item]);
    }
    else{
      if(item =="Img"){
        $('#' + item).html('<img src="'+data[item]+'" style="width:calc(100% - 250px);margin-left:40px;"/>');
      }
      else{
        $('#' + item).html(data[item]);

        chrome.storage.sync.get('SetTypeid', function (id) {
          $('#CategoryID').val(id.SetTypeid);
        });
      }
    }
  });
});
chrome.storage.sync.get('Update', function (data) {
  if (data.Update != undefined) {
    if (data.Update.IS) {
      if ($('#CancelUpd').html() == undefined) {
        $('#Pro').html($('#Product').html() + "<div class='right' id='CancelUpd'>[UPDATING]</div>");
        $('#ProdDesc').hide();
        $('#lunit').hide();
        $('#Category').hide();
        $('#Pro').css("margin-bottom", "40px");
        $('#CancelUpd').on('click', function (e) {
          e.preventDefault();

          $('#ProdDesc').show();
          $('#lunit').show();
          $('#Category').show();

          alert('Update Canceled');
          chrome.storage.sync.set({ Update: { "IS": false, "ProductID": "-1" } }, function () {
            SendMessageToExtension("SEEME");
          });

          $('#CancelUpd').remove();
          $('#Pro').css("margin-bottom", "0");
        });
      }
    }
  }
});
function SendMessageInCurrentTAB(message) {
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.tabs.sendMessage(tabs[0].id, { "command": message }, function (response) {
      console.log(response.result);
    });
  });
}
$('#Product').on('click', function () { clickedBy('Product') });
$('#ProdDesc').on('click', function () { clickedBy('ProdDesc') });
$('#Manufacturer').on('click', function () { clickedBy('Manufacturer') });
$('#Price').on('click', function () { clickedBy('Price') });
$('#Code').on('click', function () { clickedBy('Code') });
$('#Img').on('click', function () { clickedBy('Img') });
function clickedBy(id) {
  SendMessageInCurrentTAB(id);
}
chrome.runtime.onMessage.addListener(
  function (request, sender, sendResponse) {
    console.log(sender.tab ?
      "from a content script:" + sender.tab.url :
      "from the extension");
    getAndSetValues(request.greeting);
    sendResponse({ firewall: "goodbye" });
  }
);
chrome.storage.sync.get("IsFetched", function (data) {
  if (data["IsFetched"] == null) {
    $.ajax({
      url: 'https://chrome.daudt.co/AjaxCRUDJson.php?CATEGORIES=CDGXY',
      type: 'Get',
      success: function (response) {
        chrome.storage.sync.set({ IsFetched: response }, function () {
          SetOnCategories(response);
        });
      }
    });
  }
  else {
    SetOnCategories(data["IsFetched"]);
  }
});
function SetOnCategories(data) {
  var dataM = JSON.parse(data);
  var li = '';
  dataM.VAL.forEach(function (item, ind) {
    var categoryID = item.ID;
    var CatName = item.Cat;
    li += '<li class="dropdown-submenu">';
    li += '<a href="#" tabindex="-1" data-toggle="collapse" data-target="#' + categoryID + '" aria-expanded="false" aria-controls="collapseExample">'
    li += CatName;
    li += '</a>';
    li += '<ul class="collapse" id="' + categoryID + '">';
    item.SubCat.forEach(function (item, ind) {
      var SubID = item.ID;
      var SubName = item.SCat;
      li += '<li><a class="Type" href="#A" data-id="' + SubID + '">' + SubName + '</a></li>';
    });
    li += '</ul>';
    li += '</li>';
  });
  dataM.VAL1.forEach(function (item, ind) {

    var opt = '<option value="' + item.ID +'">' + item.Unit + '</option>';
    $('#Unit').html($('#Unit').html() + opt);


  });
  $('#dropdownMenu').html(li);
setTimeout(() => {
  chrome.storage.sync.get('United', function (data1) {
    if(data1.United!=undefined){
      $('#Unit').val(data1.United);
    }
  });
},10);
  reRegister();
}
function reRegister() {
  $('[data-toggle="collapse"]').on('click', function (e) {
    e.preventDefault();
    var target_element = $(this).attr("data-target");//href
    $(target_element).collapse('toggle');
    return false;
  });
  $('.Type').on('click', function (e) {
    var selected = $(this).html();
    var id = $(this).attr('data-id');
    $('#CategoryID').val(id);
    $('#SetType').html(selected);

    chrome.storage.sync.set({SetType:selected}, function (data) {
    });
    chrome.storage.sync.set({SetTypeid:id}, function (data) {
    });

    document.getElementById('SetType').click();
  });

  $('#Unit').on("change",function(){

    var unit =$('#Unit option:selected').val();
    chrome.storage.sync.set({United:unit}, function (data) {
      console.log("UNIT CHANGED :"+unit);
    });
    
  });
}
$('#Send').on('click', function () {


  


  //T_Products           1
  var Prod = {
    "IsUpdating": false,                    //IF UPDATING STATUS IS ON))                           DONE
    "UpdatingID": "",                      //IF IS Updating TRUE        ))                         DONE
    "Prod_Desc": "",                      // GOT                         ))                       _DONE____________________
    "SubCategoryID": "",                 // GOT                          ))                      /_DONE___________________\\
    "Prod_Det": "",                     // GoT                          ))                      // DONE                   //
    "User_ID": "1",                    // NO NEED                      ))                      //  NOT USABLE            //
    "Prod_Unit": "",                  // GET UNIT                     ))                      //   DONE                 //
    "ID_STY": "1"                    // NO NEED                      ))                      //    NOT USABLE          //
  };                                // ENDING TABLE PRODUCTS________))                      //                        // 
  //T_Manufacturer       2         //                                                      //                        //
  var Man = {                     //               /\\                                     \_________________________/
    "Man_Name": "",              //               // \\                  =============      \\     DONE             \\
    "Man_Link": ""                               //   \\       GOT                           \\    DONE              \\
  };
  //T_Prices             3                     //       \\               =============         \\                      \\
  var Pric = {                                                                 //               \\PRIC INIT             \\
    "ID_Prod": "",                           // ID OF T_PR\\                                    //SET IN PHP             \\ 
    "Pric_Value": "",               //        =====GOT======                                   // DONE WITH CONFUSION     \\
    "ID_Man": "",                          //ID OF T_Manufact\\                               //  SET IN PHP               \\
    "Pric_Date": Get_Date(),      //                                                         //   DONE                     //
    "Pric_user": "",                     // Price Taken By User\\                           //    DONE WITH CONFUSION      \\
    "Pric_obsolete": "1",                                                                  //     NOT USABLE               //
    "Pric_img1": ""                    // GET IMAGE FROM ON CLICK\\                       //_______________________________\\
  };//                                                                                   //________________________________//
  //T_Item_Bot           4
  var Bot = {
    "ID_Prod":"",      //SET IN PHP
    "Itm_Code":"",    //MAN_CODE
    "ID_Sup":"",     //1
    "Itm_Status":"",//1
    "ID_Man":"",   //SET IN PHP
    "Bot_Link":"" //GOT
  };
  chrome.storage.sync.get('Update', function (data) {
    
    if (data.Update != undefined) {
      if (data.Update.IS) {
        Prod.IsUpdating = true;
        Prod.UpdatingID = data.Update.ProductID;
      }
      else {
        Prod.IsUpdating = false;
        Prod.UpdatingID = -1;
      }
    }
    else {
      Prod.IsUpdating = false;
      Prod.UpdatingID = -1;
    }
  });
  Prod.Prod_Unit = $('#Unit option:selected').val();
  Prod.SubCategoryID = $('#CategoryID').val();
  
  arr.forEach(function (item, index) {
    chrome.storage.sync.get(item, function (data) {
      if (item == "Product") {
        Prod.Prod_Desc = data[item];
        
        chrome.storage.sync.get("ProductUrl", function (data) {
          Man.Man_Link = data.ProductUrl;
          Bot.Bot_Link = data.ProductUrl;
        });
      }
      else if (item == "ProdDesc") {
        Prod.Prod_Det = data[item];
      }
      else if (item == "Manufacturer") {
        Man.Man_Name = data[item];
      }
      else if (item == "Price") {
        Pric.Pric_Value = data[item];
        Pric.Pric_user = data[item];
      }
      else if (item == "Code") {
        Bot.Itm_Code = data[item];
      }
      else if (item == "Img") {
        Pric.Pric_img1 = data[item];
      }
    });
  });




  setTimeout(() => {
    if(!confirm("PLEASE CONFIRM DATA SUBMISSION \n 1) PRODUCT ="+Prod.Prod_Desc+"\n 2) PRODUCT DESCRIPTION ="
    +Prod.Prod_Det+"\n 3) UNIT ="+$('#Unit option:selected').html()+"\n 4) MANUFACTURER ="+Man.Man_Name+"\n 5) PRICE ="
    +Pric.Pric_Value+"\n 6) Product Code ="+Bot.Itm_Code+"\n 7) Category ="+$('#SetType').html())){return;}

    var Json = {"Prod":Prod,"Man":Man,"Pric":Pric,"Bot":Bot};
    console.log(Json);
    $.ajax({
      url:"https://chrome.daudt.co/AjaxCRUDJson.php?CATEGORIES=SETINDBASY",
      type:"POST",
      data:{"Pro":JSON.stringify(Json)},
      success: function(resp){
        console.log(resp);
        if(resp == 'Success')
        {
          chrome.storage.sync.clear();
          window.location.reload();
        }
      }
    });
  },500);
});
reRegister();

function Get_Date() {
  var today = new Date();
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();
  today = mm + '/' + dd + '/' + yyyy;
  return today;
}

$(document).on('keyup', function(e) {
  var code = e.keyCode;
   var IsShift = e.shiftKey;
   if(IsShift && code==80){
    clickedBy('Product');
   }
   else if(IsShift && code==68){
    clickedBy('ProdDesc')
  }
  else if(IsShift && code==77){
    clickedBy('Manufacturer');
  }
  else if(IsShift && code==67){
    clickedBy('Code');
  }
  else if(IsShift && code==49){
    clickedBy('Price');
  }
});
var isFirst = true;
$('#Edit').on('click', function(){
  if(isFirst)
  {
    arr.forEach(function (item, index) {
      chrome.storage.sync.get(item, function (data) {
        if(item!="Img"){
        $('#txt' + item).val(data[item]);
        }
        else{
          $('#txt' + item).html('<img src="'+data[item]+'" style="width:calc(100% - 250px);margin-left:40px;"/>');
        }
      });
    });


    $('.dal').hide();
    $('.txt').show();
    $('#Edit').val('SAVE EDIT');
    $('#Edit').removeClass("btn-primary").addClass("btn-danger");
  }
  else {
    chrome.storage.sync.set({Product:$('#txtProduct').val()}, function (data) {});
    chrome.storage.sync.set({ProdDesc:$('#txtProdDesc').val()}, function (data) {});
    chrome.storage.sync.set({Manufacturer:$('#txtManufacturer').val()}, function (data) {});
    chrome.storage.sync.set({Price:$('#txtPrice').val()}, function (data) {});
    chrome.storage.sync.set({Code:$('#txtCode').val()}, function (data) {});


    arr.forEach(function (item, index) {
      chrome.storage.sync.get(item, function (data) {
        if(item!="Img"){
        $('#' + item).html(data[item]);
        }
        else{
          $('#' + item).html('<img src="'+data[item]+'" style="width:calc(100% - 250px);margin-left:40px;"/>');
        }
      });
    });


    $('.dal').show();
    $('.txt').hide();
    $('#Edit').val('EDIT');
    $('#Edit').removeClass("btn-danger").addClass("btn-primary");
  }
  isFirst = !isFirst;
});
}
catch{}

